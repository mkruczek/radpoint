package pl.michalkruczek.controller;

import pl.michalkruczek.model.Player;
import pl.michalkruczek.model.Score;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mikr on 08/09/17.
 */
public class PlayerController {


    public Player setNameOfPlayer(String value) {

        Player player = new Player();

        StringBuilder firstName = new StringBuilder();
        StringBuilder lastName = new StringBuilder();

        int spaceCounter = 0;
        for (int i = 0; i < value.length(); i++) {
            if (value.charAt(i) != ' ' && spaceCounter < 1) {
                firstName.append(value.charAt(i));
            } else if (value.charAt(i) == ' ') {
                spaceCounter++;
                lastName.append(value.charAt(i));
            } else {
                lastName.append(value.charAt(i));
            }
        }

        lastName = new StringBuilder(lastName.substring(1));

        player.setFirsName(firstName.toString());
        player.setLastName(lastName.toString());

        return player;
    }

    public Double averageTime(Player player) {
        Double result = 0.00;

        List<Double> timeForOneThousandFormEveryScore = new ArrayList<>();

        for (Score score : player.getScores()) {
            timeForOneThousandFormEveryScore.add(score.getTime() * 1000 / score.getDistance());
        }

        for (Double time : timeForOneThousandFormEveryScore) {
            result += time;
        }

        return result / timeForOneThousandFormEveryScore.size();
    }

}
