package pl.michalkruczek.controller;

import pl.michalkruczek.model.Player;
import pl.michalkruczek.model.Score;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created by mikr on 08/09/17.
 */
public class Controller {


    private PlayerController playerController = new PlayerController();
    private ScoreController scoreController = new ScoreController();

    public List<Player> valuesFromDate(String method, String uri) throws FileNotFoundException {

        List<Player> players = new ArrayList<>();


        switch (method) {

            case "-f":

                File fileF = new File(uri);
                Scanner readFile = new Scanner(fileF);

                Player playerF = new Player();

                makeFile:
                while (readFile.hasNext()) {
                    playerF = playerController.setNameOfPlayer(readFile.nextLine());
                    playerF.setCountry(readFile.nextLine());
                    playerF.setScores(new ArrayList<Score>());
                    while (true) {
                        String nextLine = "";
                        if (readFile.hasNext()) {
                            nextLine = readFile.nextLine();
                        } else {
                            break;
                        }
                        if (!nextLine.equals("-")) {
                            String[] arrayWithScore = nextLine.split("\\|");
                            Score score = scoreController.singleScore(arrayWithScore);
                            playerF.getScores().add(score);
                        } else {
                            players.add(playerF);
                            playerF = new Player();
                            continue makeFile;
                        }
                    }

                }

                players.add(playerF);

                break;

            case "-d":

                File[] directory = new File(uri).listFiles();

                for (File fileD : directory) {
                    Player playerD = new Player();
                    Scanner readDirectory = new Scanner(fileD);

                    playerD = playerController.setNameOfPlayer(readDirectory.nextLine());
                    playerD.setCountry(readDirectory.nextLine());

                    playerD.setScores(new ArrayList<Score>());

                    while (readDirectory.hasNext()) {
                        String[] arrayWithScore = readDirectory.nextLine().split("\\|");
                        Score score = scoreController.singleScore(arrayWithScore);
                        playerD.getScores().add(score);
                    }

                    players.add(playerD);
                }
                break;

        }

        return players;
    }

    public Date firstDateEver(List<Player> playerList) {

        Date result = playerList.get(0).getScores().get(0).getDate();

        for (Player player : playerList) {
            ArrayList<Score> scoreList = player.getScores();
            Date helpDate = scoreController.firstScoreDateForPlayer(scoreList);
            if (helpDate.before(result)) {
                result = helpDate;
            }
        }

        return result;
    }

    public String[] topCountry(List<Player> playerList, int topNumber) {

        String[] top = new String[topNumber];

        Map<String, Integer> countryMap = new HashMap<>();

        String country = null;
        Integer counter = 1;
        for (Player player : playerList) {
            country = player.getCountry();

            if (!countryMap.containsKey(country)) {
                countryMap.put(country, counter);
            } else {
                countryMap.put(country, countryMap.get(country) + 1);
            }
        }

        Map<String, Integer> sortedMap =
                countryMap.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));


        for (int i = 0, j = sortedMap.size() - 1; i < topNumber; i++, j--) {
            top[i] = String.valueOf(sortedMap.entrySet().toArray()[sortedMap.size() - 1]);
            int secateur = top[i].indexOf('=');
            top[i] = (String) top[i].subSequence(0, secateur);
            sortedMap.remove(top[i]);
        }

        return top;
    }

    public void printTop(String[] top) {

        System.out.println("Top " + top.length + " panstwa:");

        for (String country : top) {
            System.out.println("- " + country);
        }
    }

    public List<String> checkParameters(String method, String uri) {


        List<String> result = new ArrayList<>();
        result.add("access denied");

        Scanner sc = new Scanner(System.in);
        do {
            switch (method) {
                case "-f":
                    if (uri.contains(".txt")) {
                        result.set(0,"access");
                        result.add("-f");
                    } else {
                        System.out.println("Podałeś parametr -f przeznaczony dla pliku oraz wskazałeś ściężkę do katalogu.");
                        System.out.println("Czy zmienić Twój parametr na -d. [Y/N]");
                        String answer = sc.nextLine();
                        if (answer.toUpperCase().equals("Y")) {
                            method = "-d";
                            result.set(0,"access");
                            result.add(method);
                        } else {
                            System.out.println("hmmm... zła odpowiedź, spróbuj raz jeszcze.");
                        }
                    }
                    break;
                case "-d":
                    if (!uri.contains(".txt")) {
                        result.set(0,"access");
                        result.add("-d");
                    } else {
                        System.out.println("Podałeś parametr -d przeznaczony dla katalogu oraz wskazałeś ściężkę do pliku.");
                        System.out.println("Czy zmienić Twój parametr na -f. [Y/N]");
                        String answer = sc.nextLine();
                        if (answer.toUpperCase().equals("Y")) {
                            method = "-f";
                            result.set(0,"access");
                            result.add(method);
                        } else {
                            System.out.println("hmmm... zła odpowiedź, spróbuj raz jeszcze.");
                        }
                    }
                    break;
                default:
                    System.out.println("Złe paramtery. Dla pliku jest -f , a dla katalogu -d. Proszę podaj raz jeszcze.");
                    method = sc.nextLine();

            }
        } while (result.get(0).equals("access denied"));


        return result;
    }


}
