package pl.michalkruczek.controller;

import pl.michalkruczek.model.Score;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mikr on 08/09/17.
 */
public class ScoreController {

    public Score singleScore(String[] arrayWithScore) {
        Score score = new Score();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            score.setDate(sdf.parse(arrayWithScore[0]));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        score.setDistance(Integer.valueOf(arrayWithScore[1]));
        score.setTime(Double.valueOf(arrayWithScore[2]));

        return score;
    }

    public Date firstScoreDateForPlayer(ArrayList<Score> scores) {

        Date result = null;
        Date helpDate = scores.get(0).getDate();

        if (scores.size() == 1) {
            result = scores.get(0).getDate();
        } else {
            for (int i = 1; i < scores.size(); i++) {
                if (scores.get(i - 1).getDate().before(scores.get(i).getDate())) {
                    if (scores.get(i - 1).getDate().before(helpDate)) {
                        helpDate = scores.get(i - 1).getDate();
                    }
                } else {
                    if (scores.get(i).getDate().before(helpDate)) {
                        helpDate = scores.get(i).getDate();
                    }
                }
            }

            result = helpDate;
        }

        return result;
    }

}
