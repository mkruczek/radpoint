package pl.michalkruczek.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mikr on 08/09/17.
 */
public class Score {

    private Date date;
    private Integer distance;
    private Double time;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    @Override
    public String toString() {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdfDate.format(getDate());

        return "Score{" +
                "date=" + date +
                ", distance=" + distance +
                ", time=" + time +
                '}';
    }
}
