package pl.michalkruczek.model;

import java.util.ArrayList;

/**
 * Created by mikr on 08/09/17.
 */
public class Player {

    private String firsName;
    private String lastName;
    private String country;
    private ArrayList<Score> scores;


    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ArrayList<Score> getScores() {
        return scores;
    }

    public void setScores(ArrayList<Score> scores) {
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "Player{" +
                "firsName='" + firsName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                ", scores=" + scores +
                '}';
    }
}
