import pl.michalkruczek.controller.Controller;
import pl.michalkruczek.controller.PlayerController;
import pl.michalkruczek.model.Player;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by mikr on 08/09/17.
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Controller controller = new Controller();
        PlayerController playerController = new PlayerController();

        if (args.length == 2) {

            String method = args[0];
            String uri = args[1];

            List<String> allValues = controller.checkParameters(method, uri);

            if (args.length == 2 && allValues.get(0).equals("access")) {
                method = allValues.get(1);

                List<Player> playerList = controller.valuesFromDate(method, uri);


                for (Player player : playerList) {

                    DecimalFormat df = new DecimalFormat("#.00");

                    System.out.println(player.getFirsName() + " " + player.getLastName() + " - 1000m w " + df.format(playerController.averageTime(player)) + "s.");
                }

                System.out.print("\n");

                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy,");
                System.out.println("Pierwszy wynik zarejestrowano dnia: " + sdf.format(controller.firstDateEver(playerList)));

                System.out.print("\n");

                String[] topCountry = controller.topCountry(playerList, 3);
                controller.printTop(topCountry);

            }
        } else {
            System.out.println("Zła liczba parametrów.");
        }

    }

}
